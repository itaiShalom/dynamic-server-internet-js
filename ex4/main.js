/**
 * Created by Itai on 17/01/2017.
 */
hujiwebserver = require('./hujiwebserver.js');
var gambling = {ones:0,zeros:0};
hujiwebserver.use("/gamble/1", function(req,res,next){
    if(req.method==="POST") {
        gambling.ones++;
        res.json(gambling);
    }
});
hujiwebserver.use("/gamble/0", function(req,res,next){
    if(req.method==="POST") {
        gambling.zeros++;
        res.json(gambling);
    }
});

hujiwebserver.use("/gamble/reset", function(req,res,next){
    if(req.method==="DELETE") {
        gambling.ones = 0;
        gambling.zeros = 0;
        res.json(gambling);
    }
});
hujiwebserver.use("/www/*",function(req,res,next){
    var types = {js:"application/javascript",html:"text/html",css:"text/css"};
    var path = require('path');
    fs = require('fs');
    var fullPath = (path.resolve(__dirname+"/"+req.path));
    if (types[fullPath.split('.').pop()]!==undefined) {
        fs.readFile(fullPath, 'utf8', function (err, data) {
            if (err) {
                return console.log(err);
            }
            res.set("content-type",types[fullPath.split('.').pop()]);
            res.send(data.toString());
        });
    }
});


var server = hujiwebserver.start(8081, function(err) {
    if (err) {
        console.log("game failed : could not start server " + err);
        return;
    }

});
