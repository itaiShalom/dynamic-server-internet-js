/**
 * Created by Itai on 17/01/2017.
 */
var httpRequest= new XMLHttpRequest();
function reset() {
    var url = "/gamble/reset";
    httpRequest.onreadystatechange = hideButtonsIfOK;
    httpRequest.open('DELETE', url);
    httpRequest.send();
}


function hideButtons() {
    var but0 = document.getElementById("vote0");
    var but1 = document.getElementById("vote1");
    but0.style.display = "none";
    but1.style.display = "none";
    but0.style.visibility = "hidden";
    but1.style.visibility = "hidden";
}

function finishMessage(div, ones, zeros) {
    div.appendChild(document.createElement("br"));
    div.innerHTML += ones + " users chose 1 so far (Excluding you);";
    div.appendChild(document.createElement("br"));
    div.innerHTML += zeros + " users chose 0 so far (Excluding you);";
    div.style.visibility = "visible";
    div.style.display = "block";
}
function hideButtonsIfOK() {
    if(httpRequest.readyState === XMLHttpRequest.DONE && httpRequest.status === 200){
        hideButtons();
    }
}
function post1() {
    httpRequest.onreadystatechange = alertContents1;
    var url = "/gamble/1";
    httpRequest.open('POST', url);
    httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    httpRequest.send('gambling=' + encodeURIComponent("1"));

}
function post0() {
    httpRequest.onreadystatechange = alertContents0;
    var url = "/gamble/0";
    httpRequest.open('POST', url);
    httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    httpRequest.send('gambling=' + encodeURIComponent("0"));
}

function alertContents0() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            var Answer = JSON.parse(httpRequest.responseText);
            hideButtons();
            var div= document.getElementById("resultId");
            if(Answer.ones===(Answer.zeros-1)) {
                div.innerHTML = "Tie:";
            }
            else if(Answer.ones<(Answer.zeros-1)) {
                div.innerHTML = "You lost:";
            }
            else {
                div.innerHTML = "You Won:";
            }
            finishMessage(div, Answer.ones, Answer.zeros-1);
        } else {
            alert('There was a problem with the request.');
        }
    }
}


function alertContents1() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
        if (httpRequest.status === 200) {
            var Answer = JSON.parse(httpRequest.responseText);
            //alert(Answer.ones)
            hideButtons();
            var div= document.getElementById("resultId");

            if((Answer.ones-1)===Answer.zeros){
                div.innerHTML = "Tie:";
            }
            else if((Answer.ones-1)<Answer.zeros){
                div.innerHTML = "You Won";
            }
            else{
                div.innerHTML = "You lost:";
            }
            finishMessage(div, Answer.ones - 1,Answer.zeros);

        } else {
            alert('There was a problem with the request.');
        }
    }
}