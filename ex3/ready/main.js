/**
 * Created by Itai on 22/10/2016.
 */
var PASSWORD="admin";
//var CALCULATOR = new Calc();
var LOGIN_DIV_ID = "loginID";
var PROFILE_DIV_ID ="ProfilePage";
var CALCULATOR_DIV_ID = "CalculatorPage";
var CALCULATOR_BUTTON = "Calculator"
var arrayOfCalcs =[];
/////////////////Helper Functions

//////Start of Calc object
/**
 * Constructor the Calc
 */
function Calc(i){
    var _result=0;
    var _index = i;

    /**
     * Calculator add
     * @param num - Add this number
     * @returns {*} result
     */
    this.add = function(num) {
        return addPrivate(num)
    }
    function addPrivate (num){
        return _result +=num;

    }

/* Calculator multiplication
    * @param num - number multyply by
    * @returns {*} - result
    */

    this.mul = function(num){
        return mulPrivate(num);
    }
    function mulPrivate(num){
        return _result *=num;
    }

    /**
     * Clears the value at the calculator
     * @returns {number} - zero
     */
    this.clear = function()
    {
       return clearPrivate();
    }
    function clearPrivate(){
        _result=0;
        return 0;
    }

    /**
     * To string
     * @returns {string}
     */
    this.toString =function(){
        return "Calc num: "+getIndex()+" val: "+getVal();
    }
    /**
     * getter for index
     * @returns {*}
     */
    function getIndex(){
        return _index;
    }

    /**
     * getter for value
     * @returns {number}
     */
    function getVal(){
        return _result;
    }
}
////////End of Calc object

/**
 * Hides division
 * @param divElement - The division id to hide
 */
function hideDiv(divElementID) {
    var divElement= document.getElementById(divElementID);
    divElement.style.visibility = "hidden";
    divElement.style.display = "none";
}
/**
 * Shows division
 * @param divElement - The division id to show
 */
function showDiv(divElementID) {
    var divElement= document.getElementById(divElementID);
    divElement.style.visibility = "visible";
    divElement.style.display = "block";
}
/**
 * Switch between divisions
 * @param divHide - Hide this division
 * @param divShow - Show this division
 */
function switchDivisions(divHide,divShow)
{
    hideDiv(divHide);
    showDiv(divShow);
}
/**
 * Adds a classical button
 * @param originalForm - To this form
 * @param name - The name of the button
 * @param innerHtml - what's written in it
 * @returns {Node|XML} - the button object created
 */
function addButton(originalForm, name,innerHtml)
{
    var button = originalForm.appendChild((document.createElement('a')));
    button.href = '#';
    button.className = "myButton";
    button.id = name;
    button.innerHTML = innerHtml;
    return button;
}

/**
 * add log out button
 * @param form - Which form to add the button
 * @param divPage - Which division to add the button
 */
function addLogOutButton(form,divPage)
{
    addButton(form,'logOut','Log Out').addEventListener("click", function() {

        alert("you logged out successfully");
        switchDivisions(divPage.id,LOGIN_DIV_ID);
        return;
    });
}
/**
 * Makes the starship fly!
 * @param element
 */
function myMove(element) {
    var elem = document.getElementById(element);
    var pos = 0;
    var id = setInterval(frame, 10);
    function frame() {
        if (pos == 700) {
            clearInterval(id);
            elem.style.left=0;
            switchDivisions(LOGIN_DIV_ID,PROFILE_DIV_ID);

        } else {

            pos=pos+10;

            //      elem.style.top = pos + 'px';
            elem.style.left = pos + 'px';
        }
    }
}
/**
 * Add space to the form
 * @param form - the form
 */
function addSpace(form)
{
    form.appendChild((document.createElement('Text'))).innerHTML = "&nbsp&nbsp";
}
/**
 * Creates a standard division
 * @param divID - Division ID
 */
function createDiv(divID)
{
    var newDiv = document.createElement("Div");
    newDiv.id = divID;
    newDiv.style.textAlign = "center";
    document.body.appendChild(newDiv);
    hideDiv(divID);
    return newDiv;
}


/////////////////END OF Helper Functions
/**
 * Creates the login page
 */
function createLoginPage()
{
    try{
        var divLogin = createDiv(LOGIN_DIV_ID);
        //Create FORM

        var form = document.createElement("Form");
        form.appendChild((document.createElement('Text'))).innerHTML = "Username:&nbsp&nbsp";
        var inputUser = form.appendChild((document.createElement('input')));
        inputUser.type = 'text';
        inputUser.id = 'user';
        form.appendChild((document.createElement('Text'))).innerHTML = "&nbsp&nbspPassword:&nbsp&nbsp";
        var inputPass = form.appendChild((document.createElement('input')));
        inputPass.type = 'password';
        inputPass.id = 'pass';
        addSpace(form);

        //Callback for submission
        addButton(form,'Send','Send').addEventListener("click", function(){
            var sUserInput = document.getElementById("user").value;
            var sPasswordInput = document.getElementById("pass").value;
            var inputCompare = sUserInput.localeCompare(sPasswordInput);
            var passCompare = sUserInput.localeCompare(PASSWORD);
            if(inputCompare===0 && passCompare===0){
                myMove(document.getElementById('move').id);

            }else
            {
                alert("Unknown user/password");
            }
        });

        divLogin.appendChild(form);
        form.appendChild((document.createElement('br')))
        var divImage = divLogin.appendChild(document.createElement('Div'));
        divImage.id ="move";
        divImage.className="d"
        var image = divImage.appendChild((document.createElement('img')));
        image.id = "movingImage";
        image.src = 'ship.png';
        image.setAttribute("width", "304");
        image.setAttribute("width", "228");


    }
    catch(err)
    {
        alert(err.message);
    }

}
/**
 * Creates the profile page
 */
function createProfilePage(){
    try {

        var divProfile = createDiv(PROFILE_DIV_ID);
        var form = divProfile.appendChild(document.createElement("Form"));
        addLogOutButton(form,divProfile);

        addSpace(form);
        addButton(form,CALCULATOR_BUTTON,CALCULATOR_BUTTON).addEventListener("click", function() {
            switchDivisions(PROFILE_DIV_ID,CALCULATOR_DIV_ID);
            return;
        });

//        document.body.appendChild(divProfile);

        var head= form.appendChild(document.createElement('h1')).innerHTML="Itai Shalom";;

        var image = form.appendChild((document.createElement('img')));

        image.setAttribute("high","200px");
        image.setAttribute("width","200px");
        image.src = "me.jpg";
        image.onmouseover=function(){
            image.src = "me2.jpg";
            image.setAttribute("high","200px");
            image.setAttribute("width","206px");
        };
        image.onmouseleave=function(){
            image.src = "me.jpg";
            image.setAttribute("high","200px");
            image.setAttribute("width","200px");
        };
        form.appendChild((document.createElement("P"))).innerHTML="Graduated from the Hebrew university (BSc)." +
            " Studied Electrical and computers engineering with specialization in Applied Physics " +
            "Proceeded with masters degree at computer science";
        form.appendChild((document.createElement("P"))).innerHTML="As a self-motivated and quick learner, " +
            "my field of interest is programing:<br> Java, .NET and relevant technologies of C#, C++," +
            " web applications and mobile development " +
            "Looking for a student position."
        form.appendChild((document.createElement("P"))).innerHTML="I like coding, playing soccer and basketball."
    }
    catch(err){
        alert(err.message);
    }
}
/**
 * Creates the calculator page
 */

function addAddExtraCalc(form,index){

    arrayOfCalcs[index] = new Calc(index);
    form.appendChild((document.createElement('Text'))).innerHTML = "<br><br>";
    var text = form.appendChild(document.createElement('textarea'));
    text.rows = 1;
    text.cols = 10;
    text.id = "result"+index;
    text.innerHTML = '0';

    text.disabled='true';
 //   arrayOfCalcs[index].clear();

    form.appendChild((document.createElement('Text'))).innerHTML = "<br><br>";
    addButton(form,'add:'+index,'+').addEventListener("click", function(e) {

        try {

            var id = e.target.id.split(":");
            var res = document.getElementById('result'+id[1]);
            res.value = arrayOfCalcs[id[1]].add(parseInt(document.getElementById('inputCalcField'+id[1]).value));
        }
        catch(err)
        {
            alert(err.message);
        }
    });
    addSpace(form);
    var inputCalc = form.appendChild((document.createElement('input')));
    inputCalc.type = 'text';
    inputCalc.id = 'inputCalcField'+index;

    //Checks if the input is positive number
    inputCalc.onkeyup=function()
    {
        if(isNaN(inputCalc.value) )
        {
            inputCalc.value="";
            return;
        }
        var val = parseInt(inputCalc.value)
        {
            if(val<0)
            {
                inputCalc.value="";
                return;
            }
        }
    }
    addSpace(form);
    addButton(form,'mul:'+index,'*').addEventListener("click", function(e) {
        var id = e.target.id.split(":");
        var res = document.getElementById('result'+id[1]);
        res.value = arrayOfCalcs[id[1]].mul(parseInt(document.getElementById('inputCalcField'+id[1]).value));
    });
    form.appendChild((document.createElement('Text'))).innerHTML = "<br><br>";
    addButton(form,'Clear:'+index,'Clear').addEventListener("click", function(e) {
        var id = e.target.id.split(":");
        var res = document.getElementById('result'+id[1]);
        res.value = arrayOfCalcs[id[1]].clear();
    });
}


function createCalculatorPage()
{
    var divCalc = createDiv(CALCULATOR_DIV_ID);
    var form = divCalc.appendChild(document.createElement("Form"));
    addLogOutButton(form,divCalc);
    addSpace(form);
    addButton(form,'Profile','Profile').addEventListener("click", function() {
        switchDivisions(CALCULATOR_DIV_ID,PROFILE_DIV_ID);
        return;
    });
    addSpace(form);
    addButton(form,'Calc','Add Calc').addEventListener("click", function() {
        addAddExtraCalc(form,arrayOfCalcs.length);
        return;
    });
    addAddExtraCalc(form,arrayOfCalcs.length);
    //  document.body.appendChild(divCalc);
}


/**
 * Starts the background movie
 */
function createBackgroundVideo()
{
   var vid= document.createElement("video");
    vid.muted = true;
    vid.autoplay = true;
    vid.loop=true;
    vid.id = "bgvid";
    vid.src = "web.mp4";
    vid.type = "video/mp4";
    vid.play();
    document.body.appendChild(vid);
}

/**
 * Start the program (shows the login page)
 * @constructor
 */
function MainProgram()
{
    showDiv(LOGIN_DIV_ID);
}

//Main
try {
    createLoginPage();
   createProfilePage();
    createCalculatorPage();
    createBackgroundVideo();
    MainProgram();
}catch(err)
{
    alert(err.message);
}
