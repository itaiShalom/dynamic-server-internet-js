/**
 * Created by Itai on 02/11/2016.
 */
var hujiresponsemaker  = (function hujiresponsemaker() {
    //Includes
    var fs = require("fs");
    var SUPPORTED_EXT = {
        js: "application/javascript",
        tx: "text/plain",
        html: "text/html",
        css: "text/css",
        jpg: "image/jpeg",
        png: "image/jpeg",
        gif: "image/gif",
        mp4: "mp4/video"
    }
    var BR = "\r\n";
    var SP =" ";
    var DEFAULT_RESPONSES = {
        404: {Code: 404, Message: "Not Found"},
        200: {Code: 200, Message: "OK"},
        400: {Code: 400, Message: "Bad Request"},
        500: {Code: 500, Message: "Internal Server Error"},
        505: {Code: 505, Message: "HTTP Version Not Supported"}
    }

    exports.createAnswer = function(HttpRequestObj,rootDir){
        function HttpRespond(protocol,code,message,conType,conLength,filepath) {

            this.protocol = protocol;
            this.code = code;
            this.message = message;
            this.conType = conType;
            this.conLength = conLength;
            this.filepath = filepath
            this.pipe = false;
            this.response = this.protocol+SP+this.code+SP+this.message +BR;
            this.setPipe = function () {
                this.pipe = true;
                this.response +="Content-Type: "+conType +BR+"Content-Length: "+
                    conLength +BR+BR;
            }
            this.getResponse=function(){
                return this.response;
            }
            this.isPiped = function () {
                return this.pipe;
            }
        }
        if (HttpRequestObj.badRequest ===true){
            return createResponseFactory(400,HttpRespond,"null",HttpRequestObj);
        }
        if (HttpRequestObj.badHTTPVersion ===true){
            return createResponseFactory(505,HttpRespond,"null",HttpRequestObj);
        }

        switch (HttpRequestObj.request){
            case "GET":
                return GET_FUNCTION(rootDir,HttpRequestObj,HttpRespond);
                break;
            default:
                return prepareNotFound(HttpRespond,"null",HttpRequestObj);
                break;
        }
    }

    function createResponseFactory(index,HttpRespond,conType,HttpRequestObj,contentLength,filePath)
    {
        return new HttpRespond(HttpRequestObj.protocol,DEFAULT_RESPONSES[index].Code,
            DEFAULT_RESPONSES[index].Message,conType,contentLength,filePath);
    }

    function prepareNotFound(HttpRespond,conType,HttpRequestObj){
        if(HttpRequestObj.request === "GET"){
            return new createResponseFactory(404,HttpRespond,"null",HttpRequestObj);
        }
        else
        {
            // for any request that isn't GET.
            return new createResponseFactory(400,HttpRespond,"null",HttpRequestObj);
        }
    }

    function prepareOK(HttpRespond,conType,filePathReady,size,HttpRequestObj)
    {
        var temp= createResponseFactory(200,HttpRespond,conType,HttpRequestObj,size,filePathReady);
        temp.setPipe();
        return temp;
    }

    function GET_FUNCTION(rootDir,HttpRequestObj,HttpRespond)
    {
        var filepath = HttpRequestObj.path;
        if(filepath[0]==="/" || filepath[0]==="\\") {
            filepath = filepath.substring(1);
            if(filepath.includes("..")){
                return prepareNotFound(HttpRespond,SUPPORTED_EXT[match],HttpRequestObj);
            }
            var filePathReady = rootDir + filepath;
            var pat = /\.[0-9a-z]+$/i;
            var match = pat.exec(filePathReady);
            match = match.toString().substring(1);
            var ans= fileExists(filePathReady);
            if(ans==-1|| typeof  SUPPORTED_EXT[match]==="undefined")
            {
                    return prepareNotFound(HttpRespond, SUPPORTED_EXT[match], HttpRequestObj);
            }
            else
            {
                return prepareOK(HttpRespond,SUPPORTED_EXT[match],filePathReady,ans,HttpRequestObj);
            }
        }
    }
    function fileExists(filePath)
    {
        try
        {
            var isFile= fs.statSync(filePath);
            return isFile["size"];
        }
        catch (err)
        {
            return -1;
        }
    }
})();