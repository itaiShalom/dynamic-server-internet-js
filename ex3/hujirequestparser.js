/**
 * Created by Itai on 01/11/2016.
 */
var hujirequestparser  = (function hujirequestparser() {
    var net = require('net');
    //Supported Protocols
    var HTTP1_0 = "HTTP/1.0";
    var HTTP1_1 = "HTTP/1.1";
    var EOL = /[\r\n|\r|\n|\n\r]/;
    var SOL = /[^|,]/;
    var REQUEST_LINE_LENGTH = 3;
    var NOT_FOUND_MESSAGE="Not Found";

    exports.parse = function (message,rootDir) {
        function HttpRequest(request, protocol, path, host,connectionType) {
            this.request = request;
            this.protocol = protocol;
            this.path = path;
            this.host = host;
            this.connectionType = connectionType;
            this.badRequest = false;
            this.badHTTPVersion=false;
            this.setBadRequest = function () {
                this.badRequest = true;
            }
            this.setBadHTTP = function () {
                this.badHTTPVersion = true;
            }
        }
        var HttpRequestObj = parseHeader(message,rootDir,HttpRequest);
        return HttpRequestObj;
    }

    var _requestLine;
    var _connectionType;
    var _host;
    var _closeConnection;
    var _pathToFile;

    function parseHeader(message,rootDir,HttpRequest)
    {
        _requestLine = parseFirstLine(message);
        if(typeof _requestLine === "undefined")
        {
            _requestLine = {req:"",pathToFile:"",protocol:HTTP1_1};
            var HttpRequestObj = new HttpRequest(_requestLine.req,_requestLine.protocol,_requestLine.pathToFile,
                "","");
            HttpRequestObj.setBadRequest();
            return HttpRequestObj;
        }
        else if(_requestLine.protocol==="null")
        {
            var HttpRequestObj = new HttpRequest(_requestLine.req,HTTP1_1,_requestLine.pathToFile,
                "","");
            HttpRequestObj.setBadHTTP();
            return HttpRequestObj;
        }
        _host = getHost(message,_requestLine.protocol);
        if(_host==="null"){
            var HttpRequestObj = new HttpRequest(_requestLine.req,_requestLine.protocol,_requestLine.pathToFile,
                _host,"");
            HttpRequestObj.setBadRequest();
            return HttpRequestObj;
        }
        _connectionType = getConnection(message);

        var HttpRequestObj = new HttpRequest(_requestLine.req,_requestLine.protocol,_requestLine.pathToFile,_host,_connectionType);
        return HttpRequestObj;

    }

    function getHost(message, protocolVersion)
    {
        try{
            return findAnswerForRegex("Host:",message);
        }catch (e){
            if(protocolVersion === HTTP1_0){
                // it's all cool
                return "";
            }
            else{
                return "null"
            };
        }

    }

    function findAnswerForRegex(Req,message){
        var originalRegex = new RegExp(Req+".*");
        var regex = SOL.source+originalRegex.source+EOL.source;
        var pat = new RegExp(regex);
        var match = pat.exec(message);
        var stripedEOL = match.toString().replace(EOL,"");
        var result = stripedEOL.toString().replace(Req+" ","")
        return result;
    }

    function getConnection(message)
    {
        try {
            return findAnswerForRegex("Connection:", message);
        }catch(err){
            return "null";
        }
    }

    function parseFirstLine(message)
    {

            var pat = new RegExp(/^(.*)$/m);
            var firstLine = pat.exec(message);
            var tokens = firstLine[0].toString().split(" ");
            var request = tokens[0]; // will bw checked later
            var path = tokens[1]; // will bw checked later
            var protocoltype = tokens[2];

            if(tokens.length !==REQUEST_LINE_LENGTH)
            {
                return undefined; //ACT as bad request
            }

            if (protocoltype !== HTTP1_1 && protocoltype !== HTTP1_0) {
                protocoltype = HTTP1_1;
                return {req: request, pathToFile: path, protocol: "null"};
            }
            return {req: request, pathToFile: path, protocol: protocoltype};
        }


})();