/**
 * Created by Itai on 27/10/2016.
 */
var net = require('net');
var fs = require('fs');
var counter =0;
function printDiff(got,exp )
{
    console.log("~~~~~~~~")
    console.log("=====Expected=====");
    console.log(exp.toString());
    console.log("=====got=====");
    console.log(got.toString());
    console.log("~~~~~~~~")
}
function connection(data )
{
 //   console.log("Connecting test: "+data)
}

function getData(data )
{
 //   console.log("getting data test: "+data)
}

var response= "";
var read=0;
var client = new net.Socket();

client.connect(80, 'localhost', function () {
    client.write('GET /index.html HTTP/1.1\r\nHost: localhost"\r\nConnection: close\r\n');
    connection(0);
});
client.on('data', function (data) {
    getData(0);
    read++;
    response += data.toString();
    if (read==2) {
        fs.readFile(__dirname+'\\first.txt', 'utf8', function (err, comp) {
            if (err) {return console.log(err);}
            if (comp === response) {console.log("0) Test 200 Success, regular");counter++;}
            else {console.log("Test 200 FAILED, regular");printDiff(data,comp);}
        });
    }
});

/////////////////////////////////////

var response2= "";
var read2=0;
var client2 = new net.Socket();

client2.connect(80, 'localhost', function () {
    client2.write('GET /indexz.html HTTP/1.1\r\nHost: localhost"\r\nConnection: close\r\n');
    connection(1);
});
client2.on('data', function (data) {
    getData(1);
    read2++;
    response2 += data.toString();
    if (read2==1) {
        fs.readFile(__dirname+'\\second.txt', 'utf8', function (err, comp) {
            if (err) {return console.log(err);}
            if (comp === response2) {console.log("1) Test 404 Success, bad path to html");counter++;}
            else {console.log("Test 404 FAILED, bad path to html");printDiff(data,comp);}
        });
    }
});

///////////////////////////////

var response3= "";
var read3=0;
var client3 = new net.Socket();

client3.connect(80, 'localhost', function () {
    client3.write('GET /index.html HTTP/1.1\r\nConnection: close\r\n');
    connection(2);
});
client3.on('data', function (data) {
    getData(2);
    read3++;
    response3 += data.toString();


    if (read3==1) {
        fs.readFile(__dirname+'\\third.txt', 'utf8', function (err, comp) {
            if (err) {return console.log(err);}
            if (comp === response3) {console.log("2) Test 400 Success (no host)");counter++;}
            else {console.log("Test 400 FAILED (no host)");printDiff(data,comp);}
        });
    }
});

///////////////////////////////

var response4= "";
var read4=0;
var client4 = new net.Socket();

client4.connect(80, 'localhost', function () {
    client4.write('GET /index.html HTTP/1.12\r\nHost: localhost"\r\nConnection: close\r\n');
    connection(3);
});
client4.on('data', function (data) {
    getData(3);
    read4++;
    response4 += data.toString();
    if (read4==1) {
        fs.readFile(__dirname+'\\forth.txt', 'utf8', function (err, comp) {
            if (err) {return console.log(err);}
            if (comp === response4) {console.log("3) Test 505 Success, bad protocol");counter++;}
            else {console.log("Test 505 FAILED, bad protocol");printDiff(data,comp);}
        });
    }
});

///////////////////////////////


var response5= "";
var read5=0;
var client5 = new net.Socket();

client5.connect(80, 'localhost', function () {
    client5.write('GETS /index.html HTTP/1.1\r\nHost: localhost"\r\nConnection: close\r\n');
    connection(4);
});
client5.on('data', function (data) {
    getData(4);
    read5++;
    response5 += data.toString();
    if (read5==1) {
        fs.readFile(__dirname+'\\fith.txt', 'utf8', function (err, comp) {
            if (err) {return console.log(err);}
            if (comp === response5) {console.log("4) Test 400 Success, Bad command");counter++;}
            else {console.log("Test 400 FAILED, Bad command");printDiff(data,comp);}
        });
    }
});

///////////////////////////////

var response6= "";
var read6=0;
var client6 = new net.Socket();

client6.connect(80, 'localhost', function () {
    client6.write('GET /index.html HTTP/1.0\r\nConnection: close\r\n');
    connection(5);
});
client6.on('data', function (data) {
    getData(5);
    read6++;
    response6 += data.toString();

    if (read6==2) {
        fs.readFile(__dirname+'\\sixth.txt', 'utf8', function (err, comp) {
            if (err) {return console.log(err);}
            if (comp === response6) {console.log("5) Test 200 Success, HTTP 1.0 No Host");counter++;}
            else {console.log("Test 200 FAILED, HTTP 1.0 No Host");printDiff(data,comp);}
        });
    }
});



///////////////////////////////

/* fs.appendFile("C:\\Users\\Itai\\Documents\\internet2\\ex3\\third.txt", response3, function(err) {
 if(err){return console.log(err);}
 });
 */