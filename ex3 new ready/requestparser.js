/**
 * Created by Itai on 07/01/2017.
 */

var requestparser  = (function requestparser() {
    var REQUEST_LINE_LENGTH = 3;
    var HTTP1_0 = "HTTP/1.0";
    var HTTP1_1 = "HTTP/1.1";
    var url = require('url');
    var PROTOCOL_VERSION = "protocol_version"
    var BODY = "body";
    var QUERY = "query";
    var COOKIE = "cookie";
    var PROTOCOL = "protocol";
    var METHOD = "method";
    var supportedMethods =["GET","POST","PUT","DELETE","OPTIONS","TRACE HTTP"];
    var HOST = "host";
    var PATH = "path";
    var CONTENT_TYPE = "content-type";
    var EOL = /[\r\n|\r|\n|\n\r]/;


    exports.parse = function (httpRequestString) {
        var map={};
        function getFromMap(k) {
            return map[k];
        };

        function getHost() {
            var fullHost =  getFromMap(HOST);
           try {
               var splited = fullHost.split(":");
               if (splited.length > 1)
                   map[HOST] = splited[0];
           }catch(e){
               map[HOST] = undefined;
           }
               return map[HOST];
        };
        function getCookie(){
            var cookie={};
            var ans = getFromMap(COOKIE)
            if (ans !== undefined) {
                var splitted = ans.split("; ")
                for (var i=0;i<splitted.length;i++){
                    var splitted_2 = splitted[i].split("=");
                    cookie[splitted_2[0]] = splitted_2[1].toString();
                }
            }
            return cookie;
        }
        var text = httpRequestString;

        function parseFirstLine (message)
        {
            var pat = new RegExp(/^(.*)$/m);
            var firstLine = pat.exec(message);
            var tokens = firstLine[0].toString().split(" ");
            var request = tokens[0];
            var path = tokens[1];
            var protocoltype = tokens[2];

            if(tokens.length !==REQUEST_LINE_LENGTH)
            {
                return undefined; //ACT as bad request
            }

            if (protocoltype !== HTTP1_1 && protocoltype !== HTTP1_0) {
                protocoltype = HTTP1_1;
                return {req: request, pathToFile: path, protocol: "null"};
            }
            return {req: request, pathToFile: path, protocol: protocoltype};
        }

        function parseHeader (message) {
            var _requestLine = parseFirstLine(message);
            map[PROTOCOL_VERSION] = _requestLine.protocol;
            map[PROTOCOL] = _requestLine.protocol.split("/")[0].toLowerCase();
            if(supportedMethods.indexOf(_requestLine.req)>=0)
                map[METHOD] = _requestLine.req;

            var parsedUrl = url.parse(_requestLine.pathToFile, true, true);
            for(var name in parsedUrl.query) {

                map[name] = parsedUrl.query[name];

            }
            map[QUERY] = parsedUrl.query;

            map[PATH] = parsedUrl.pathname;

            var linesYon = message.toString().split("\n\r");
            if(linesYon.length==1)
            {
                map[BODY] =null;
            }
            else
            {
                try {
                    map[BODY]= JSON.parse(linesYon[1]);
                } catch (e) {
                    map[BODY]= linesYon[1];
                }

            }

            var lines = linesYon[0].split(EOL);
            for (var i=1;i<lines.length;i++)
            {
                var splitted = lines[i].split(": ");
                if (splitted !== undefined && splitted.length==2)
                {
                    map[splitted[0].toString().toLowerCase()] = splitted[1].toString().toLowerCase();
                }
            }
        }

        parseHeader(text);

        var request = {
            params:{},
            protocol:getFromMap(PROTOCOL),
            protocolVersion:getFromMap(PROTOCOL_VERSION),
            method:getFromMap(METHOD),
            host:getHost(),
            path:getFromMap(PATH),
            query:getFromMap(QUERY),
            body:getFromMap(BODY),
            cookies:getCookie(),

            param:function(name){
                if(this.params.hasOwnProperty( name)){
                    return this.params[name];
                }
                if(this.query[name]!==undefined){
                    return this.query[name];
                }
                return undefined;
            },
            get:function(IndexString){
                return getFromMap(IndexString.toLowerCase())
            },
            is:function(value) {
                if (getFromMap(CONTENT_TYPE) === undefined) {
                    return false;
                }
                var answer = getFromMap(CONTENT_TYPE);
                var splitDot = answer.split(";");
                var compTo="";
                if (splitDot.length>1){
                    compTo=splitDot[0];
                }
                else
                    compTo =answer;
                if (compTo === value) {
                    return true;
                }
                var temp = answer.split(";");
                var splittedFromMap;
                if (temp.length>1)
                    splittedFromMap = temp[0].split("/");
                else
                    splittedFromMap = answer.split("/");
                var splittedFromUser = value.split("/");
                if (splittedFromUser.length==1)
                {
                    return value === splittedFromMap[1];
                }
                return ((splittedFromMap[0]==splittedFromUser[0]) &&
                (splittedFromUser[1]=='*'));
            }
        };

        return request;
    }

})();