/**
 * Created by Yonatan on 12/01/2017.
 */
var requestparser = require("./reqestparser.js");
var responseMaker = require("./httpResponseMaker");
var httpString = "";
/*
 getting the string somehow
 */
var req = requestparser.parse(httpString);
var res = responseMaker.createResponse(socket, req);
function searchAndExecute(req, res, i) {
    var found = true;
    if(i+1>commands.length){
        found = false;
    }
    var indexOfFound = -1;
    for(var j = i; j<commands.length; j++){
        req.params = {}; // emptying the params
        var splitedCommand = commands[i].command.toString().split('\\'); // splitting with '\' - the extra \ is for escaping
        var splitedUrl = req.url.toString().split('\\'); // splitting with '\' - the extra \ is for escaping
        if(splitedCommand.length > splitedUrl.length){
            found = false;
        }
        else{
            for(var k = 0; k<splitedCommand.length; k++){
                if(splitedCommand[k] === splitedUrl){
                    continue;
                }else if(splitedCommand[k].toString().charAt(0) === ':'){
                    req.params[splitedCommand[k].toString().substr(1/*the second argument is optional, defult to the length*/)] = splitedUrl[k];
                }else {
                    found = false;
                }
            }
        }
        if(found){
            indexOfFound = j;
            break;
        }
    }
    if(found){
        var mw = commands[indexOfFound].middleware;
        mw(req, res, function(){
            // this is the next funtion - notice how simple it becomes!!!!
            searchAndExecute(req, res, indexOfFound +1);
        })
    }else{
        // did'nt found matching command. check moodle to see what's the propper action. my guess:
        // something like res.status(404); res.send(" 404 page not found");
    }
}
searchAndExecute(req, res, 0);