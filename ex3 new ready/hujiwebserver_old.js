/**
 * Created by Itai on 26/10/2016.
 */
var hujiwebserver = (function hujiwebserver() {
    var net = require('net');
    var fs = require('fs');
    var hujiresponsemaker = require('./hujiresponsemaker');
    var hujirequestparser = require('./hujirequestparser');
    var requestparser = require ('./requestparser');
    var HTTP1_0 = "HTTP/1.0";
    var HTTP1_1 = "HTTP/1.1";
    var TIME_OUT= 2000;
    var myError;
    var HOST = 'localhost';
    var serverObj = function (port, rootFolder, callback) {
        var myError;
        try {
            var properties = {};
            Object.defineProperty(properties, '_port',
                {
                    value: port,
                    writable: false
                });
            Object.defineProperty(properties, '_rootDir',
                {
                    value: rootFolder,
                    writable: false
                });
            var server = net.createServer(function (client) {
                // when this code is run, the connection has been established
                var id = client.remoteAddress + ':' + client.remotePort;
                //   console.log('A new connection was made:', id);
                //    client.write("hi from server");
                client.on('data', function (data) {
                   // console.log('Client:', data.toString("UTF-8"));
               //     console.log("result of IS!!!!!!!!  is "+data)
                    var req = requestparser.parse(data);
                   // console.log("==============================");
                 //   console.log("result of IS!!!!!!!!  is "+data)

                 //   console.log("==============================");
                    var HttpRequestObj = hujirequestparser.parse(data, properties._rootDir);
                    if (HttpRequestObj.connectionType.trim() === "keep-alive")
                    {
                        client.setKeepAlive(true);
                    }
                    var HttpResponseObj = hujiresponsemaker.createAnswer(HttpRequestObj, properties._rootDir);
                    var strResponse;

                    client.write(HttpResponseObj.getResponse())
                    if(HttpResponseObj.isPiped())
                    {
                        var fileAsAstream = fs.createReadStream(HttpResponseObj.filepath);
                        fileAsAstream.on('open', function () {
                            // This just pipes the read stream to the response object (which goes to the client)
                            fileAsAstream.pipe(client);
                        });
                        fileAsAstream.on('close', function () {
                            closeCheck(HttpRequestObj, HttpResponseObj);
                        });
                    }else
                    {
                        closeCheck(HttpRequestObj, HttpResponseObj);
                    }
                });

                function closeCheck(HttpRequestObj, HttpResponseObj) {
                    if (HttpRequestObj.connectionType.trim() === "close")
                    {
                        client.end();
                    }
                    else if (HttpResponseObj.protocol === HTTP1_0 && HttpRequestObj.connectionType === "null")
                    {
                        client.end();
                    }
                    else
                    {
                        client.setTimeout(TIME_OUT);
                        client.on('timeout', function () {
                            client.end();
                        });
                    }
                }
                client.on('end', function () {
                });
            });

            server.listen(properties._port, HOST);
            this.stop = function (callback)
            {
                server.close();
                // STOP THE SERVER
                server.on('close',callback);
            }
        }
        catch (e){
            myError = e;
        }
        finally {
            callback(myError);
        }
    };

    exports.start= function(port,rootFolder,callback) {
        return new serverObj(port, rootFolder, callback);
    }
})();
