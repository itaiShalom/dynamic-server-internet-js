/**
 * Created by Itai on 06/01/2017.
 */
var net = require('net');
var requestParser = require("./requestparser.js");
var responseMaker = require("./httpResopnseMaker.js");
var HOST = 'localhost';
var HTTP1_1 = "HTTP/1.1";
var TIME_OUT = 10000;
var TIME_OUT_SOCKET = 25000;
module.exports = {

    commands:[],

    use: function(c,mw){

        if(mw===undefined)
        {
            mw=c;
            c="/";
        }
        this.commands.push({command:c,middleware:mw});
        return this;
    },

    start :function(p,c){
        var allCommands = this.commands;
        var server = net.createServer(function (socket) {
            socket.on('error', function (exeption) {
               socket.destroy();
            });
            socket.setTimeout(TIME_OUT_SOCKET,function(){
                socket.end();
            });
            var acc="";
            var waitingForMore=false;
            var length =0;

            var header ="";
            var req = null;
            socket.on('data', function (data) {

                try {

                    if (waitingForMore) {

                        acc += data;
                        if (acc.length < length) {
                            return;
                        } else {
                            waitingForMore = false;

                        }
                    } else {
                        var tempData = data.toString();
                        var chunk = data.toString().split("\r\n\r\n");
                        header = chunk[0];

                        var tempReq = requestParser.parse(header);
                        length = tempReq.get("content-length");

                        if(length!==undefined)
                        {
                        if (chunk.length > 1) {
                                if(chunk[1] ==="")
                                {
                                    waitingForMore = true;
                                    return;
                                }

                                if (chunk[1] !== "") {
                                    //Long data

                                    for (var i = 1; i < chunk.length; i++) {

                                        acc += chunk[i];
                                        if (i + 1 < chunk.length) {
                                            acc += "\r\n\r\n";
                                        }
                                    }
                                    if (acc.length < length) {

                                        waitingForMore = true;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                    var protocolToRespone="";
                    var text =header + "\n\r" + acc;

                    req = requestParser.parse(text);
                }catch(e)
                {
                    if(req!==null) {
                        if (req.protocolVersion!==undefined)
                            protocolToRespone = req.protocolVersion;
                        protocolToRespone=HTTP1_1;
                    }
                    req = null;
                }
                if (req!==null) {
                    if (req.get("method") === undefined|req.host===undefined | req.protocolVersion==="null") {  //Check supported Method and if host defined
                        if (req.protocolVersion!=="null")
                            protocolToRespone = req.protocolVersion;
                        else
                            protocolToRespone=HTTP1_1;
                        req = null;
                    }
                }
                var res = responseMaker.createResponse(socket, req,protocolToRespone);

                function searchAndExecute(req, res, i) {
                    var found = true;
                    if (i + 1 > allCommands.length) {
                        found = false;
                    }
                    var indexOfFound = -1;
                    for (var j = i; j < allCommands.length; j++) {
                        req.params = {}; // emptying the params
                        var splittedCommand = allCommands[j].command.toString().substr(1, allCommands[j].command.length)
                            .split('/');
                        var splitedUrl = req.path.toString().substr(1, req.path.length).split('/');
                        if(splittedCommand.length === 1 &&splittedCommand[0]===""){
                            found = true;
                            indexOfFound=j;
                            break;
                        }
                        if (splittedCommand.length > splitedUrl.length) {
                            found = false;
                        }
                        else {
                            var paramLoc = 0;
                            for (var k = 0; k < splittedCommand.length; k++) {
                                if (splittedCommand[k] === splitedUrl[k]) {
                                    found = true;
                                    continue;
                                } else if (splittedCommand[k].toString().charAt(0) === ':') {

                                    req.params[splittedCommand[k].toString().substr(1)] = splitedUrl[k];
                                    found = true;
                                } else if (splittedCommand[k] === "*") {
                                    if (k == splittedCommand.length - 1) {
                                        req.params[paramLoc] = "";
                                        for (var z = k; z < splitedUrl.length; z++)
                                            req.params[paramLoc] += splitedUrl[z] + "/";
                                        req.params[paramLoc] = req.params[paramLoc].substr(0, req.params[paramLoc].length - 1);


                                        found = true;
                                        break;
                                    }
                                    paramLoc++;
                                } else {
                                    found = false;
                                    break;
                                }
                            }
                        }
                        if (found) {
                            indexOfFound = j;
                            break;
                        }
                    }
                    if (found) {
                        var mw = allCommands[indexOfFound].middleware;
                        setTimeout(function () {
                            if (!res.isSend) {
                                res.status(404);
                                res.send(" 404 page not found");
                            } else {
                                res.isSend = false;
                            }
                        }, TIME_OUT);
                        mw(req, res, function () {
                            // this is the next funtion - notice how simple it becomes!!!!
                            searchAndExecute(req, res, indexOfFound + 1);
                        })

                    } else {
                        // did'nt found matching command. check moodle to see what's the propper action. my guess:
                        // something like res.status(404); res.send(" 404 page not found");
                        res.status(404);
                        res.send(" 404 page not found");

                    }
                }
                try {
                    searchAndExecute(req, res, 0);
                }catch(e) {
                    if (req === null) {
                        res.status(400);
                        res.send(" Bad Request");
                    } else {
                        res.status(500);
                        res.send(" Server Error");
                    }
                }
            })

        });


        try {
            server.listen(p, HOST);
            c();
        }
        catch(e){
            c("Server Could'nt Start!");
        }
        this.stop = function (callback)
        {
            server.close();

        }
        return {
            port: p,
            stop: function()
            {
                server.close();

            }
        }

    }
};