/**
 * Created by Yonatan on 04/01/2017.
 */
var net = require("net");

module.exports =
{
    createResponse: function(socket, request,protocolToRespone)
    {
    var httpResonse;
    httpResonse = {
        isSend:false,
        defaultProtocol:protocolToRespone,
        code: 200,
        text: "OK",
        headers: {},
        cookies: [],
        set: function (field, value) {
            if (value !== undefined) {
                this.headers[field] = value;
            } else {

                if (typeof field === "object") {
                    for (var key in field) {
                        if (!field.hasOwnProperty(key)) {
                            //The current property is not a direct property of p
                            continue;
                        }
                        this.headers[key] = field[key];
                    }
                }
            }

        },

        status: function (code) {
            this.code = code;
            var getText = function (code) {
                switch (code) {
                    case 200:
                        return "OK";
                    case 400:
                        return "Bad Request";
                    case 404:
                        return "Not Found";
                    case 301:
                        return "Moved Permanently";
                    case 302:
                        return "Moved Temporarily ";
                    case 303:
                        return "See Other";
                    case 500:
                        return "Server Error";
                }
            };
            this.text = getText(code);
            return this;
        },
        get: function (header) {
            if (this.headers.hasOwnProperty(header)) {
                return this.headers[header];
            } else {
                // maybe create error
                return null;
            }

        },
        cookie: function (n, v) {
            console.warn("check   name = "+n + " value = "+v)
            var cookie = {name: n, value: v};
            this.cookies.push(cookie);
        },

        send: function (body) {
            if(typeof body === 'object'){
                this.json(body);
            }
            var s = this.createMassage();
            s += ("\r\n" + body);
           try{
            socket.write(s);
            this.isSend=true;
            socket.end();
           }catch(e){
               return;
           }
        },
        json: function (body) {
            var json = JSON.stringify(body);
            this.send(json);
        },
        createMassage: function () {
            if(request === null){
                this.status(400);
            }
            var s = "";

            if(request!==null)
                s +=request.protocolVersion;// request.getFullProtocol().toUpperCase();
            else
                s +=this.defaultProtocol
                // naybe add version if it's not inside the protocol
            s += " " + this.code + " " + this.text + "\n";
            if(request === null){
                return s;
            }
            for (var header in this.headers) {
                if (this.headers.hasOwnProperty(header)) {
                    s += header + ": " + this.headers[header] + "\n";
                }
            }
            for (var i = 0; i < this.cookies.length; i++) {
                s += "Set-Cookie: " + this.cookies[i]["name"] + "=" + this.cookies[i]["value"] + "\n";
            }
            return s;
        }
    };
    return httpResonse;
    }

};