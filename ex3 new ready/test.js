/**
 * Created by Itai on 27/10/2016.
 */
var hujiwebserver = require('./hujiwebserver.js');


hujiwebserver.use('/hello/world', function (req, res, next) {
    res.set("content-type"," text/plain");
    res.send('hello world');
});


hujiwebserver.use('/add/:n/:m', function (req, res, next) {
    res.set("content-type","application/javascript");
    res.json({result:req.params.n*req.params.m});
});


hujiwebserver.use('/filez/*', function (req, res, next) {
    var types = {js:"application/javascript",html:"text/html",css:"text/css"};
    var path = require('path');
    fs = require('fs');
    var fullPath = (path.resolve(__dirname+"/filez/"+req.params[0]))
    if (types[fullPath.split('.').pop()]!==undefined) {
        fs.readFile(fullPath, 'utf8', function (err, data) {
            if (err) {
                return console.log(err);
            }
            res.set("content-type",types[fullPath.split('.').pop()]);
            res.send(data.toString());
        });
    }
});


var server = hujiwebserver.start(8080, function(err) {

    if (err) {
        console.log("test failed : could not start server " + err);
        return;
    }

    console.log("server successfully listening to port " + 8080);
    console.log("starting test");

});

